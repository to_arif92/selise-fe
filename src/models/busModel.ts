export class BusModel {
    id = 0
    name: string
    departureTime: string
    arrivalTime: string
    startLocation: string
    endLocation: string
    isAvailable: boolean
    hasAc: boolean
    cost: number
    seats: Array<Array<number>> = [
        [0, 1], [2, 3], [4, 5], [6, 7], [8, 9], [10, 11], [12, 13], [14, 15], [16, 17], [18, 19], [20, 21], [22, 23], [24, 25], [26, 27]
    ]
    bookedMaleIndexes: Array<number> = [2, 9]
    bookedFemaleIndexes: Array<number> = [1, 6, 7]
    blockedIndexes: Array<number> = [0, 3]
    availableIndexes: Array<number> = []
    selectedIndexes: Array<number> = []
    soldMaleIndexes: Array<number> = [10, 12]
    soldFemaleIndexes: Array<number> = [14]

    private seatsWithoutPair: Array<number> = [];


    constructor(name: string, departureTime: string, arrivalTime: string, startLocation: string, endLocation: string, isAvailable: boolean, hasAc: boolean, cost: number) {
        this.name = name;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.isAvailable = isAvailable;
        this.hasAc = hasAc;
        this.cost = cost;
        this.setAllSeats()
    }

    setAllSeats() {
        this.seats.forEach(pair => pair.forEach(i => this.seatsWithoutPair.push(i)));
    }

    getAvailableIndexes() {
        return this.seatsWithoutPair.filter(i => !this.bookedMaleIndexes.includes(i) && !this.bookedFemaleIndexes.includes(i) && !this.blockedIndexes.includes(i) && !this.soldFemaleIndexes.includes(i) && !this.soldMaleIndexes.includes(i))
    }
}