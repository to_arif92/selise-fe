export class FilterModel {
    from: string
    to: string
    journeyDate: string
    returnDate: string

    constructor(from: string, to: string, journeyDate: string, returnDate: string) {
        this.from = from;
        this.to = to;
        this.journeyDate = journeyDate;
        this.returnDate = returnDate;
    }
}