import Vue from 'vue'
import Vuex from 'vuex'
import {FilterModel} from "@/models/filterModel";
import {BusModel} from "@/models/busModel";
import {Subject} from "rxjs";

Vue.use(Vuex)

// todo: replace by getter argument, passed from Bus card
const busTripIndex = 0

export default new Vuex.Store({
  state: {
    filter: new FilterModel('', '', '', ''),
    busTrips : new Array<BusModel>(),
    busTripsFiltered : new Array<BusModel>(),
    activeBusTripId: 0,
    activeBusTrip: new BusModel('', '', '', '', '', false, false, 0),
    /* Observable */
    observableSeatSelectionList$: new Subject<Array<number>>(),
    observableAddedToSelections$: new Subject<number>(),
    observableRemovedFromSelections$: new Subject<number>(),
    observableModalFlag$: new Subject<boolean>(),
    /* SessionStorage = active tab = let active user */
    activeUserId: <number>Number(sessionStorage.getItem('activeSessionId')) || -1, // generated random number for each session
    /* Database = LocalStorage */
    seatSelectionByActiveUserId: <{ userId: number; selectionMap: string }>JSON.parse(<string>localStorage.getItem('seatSelectionByActiveUserId')) || null,
    seatSelectionOfAllUserList: <Array<{ userId: number; selectionMap: string }>>JSON.parse(<string>localStorage.getItem('seatSelectionOfAllUserList')) || null,
  },
  mutations: {
    setActiveFilter (state, value) {
        Object.assign(state.filter, value)
    },
    setResult (state, value) {
        state.busTripsFiltered = Array.from(value)
    },
    setActiveBusTrip (state, value) {
        state.activeBusTrip = value
    },
    addSeatSelection (state, value) {
        state.activeBusTrip.selectedIndexes.push(value)
        /* RxJS */
        state.observableAddedToSelections$.next(value)
        state.observableSeatSelectionList$.next(state.activeBusTrip.selectedIndexes)
    },
    removeSeatSelection (state, value) {
        const indexWithinSelection = state.activeBusTrip.selectedIndexes.indexOf(value)
        state.activeBusTrip.selectedIndexes.splice(indexWithinSelection, 1)
        /* RxJS */
        state.observableRemovedFromSelections$.next(value)
        state.observableSeatSelectionList$.next(state.activeBusTrip.selectedIndexes)
    },
    clearSeatSelection (state) {
        state.activeBusTrip.selectedIndexes = []
    },
    setActiveSessionId (state) {
        if (Number(sessionStorage.getItem('activeSessionId')) !== -1) return
        // set SessionId if not already
        sessionStorage.setItem('activeSessionId', Math.random().toString())
        state.activeUserId = Number(sessionStorage.getItem('activeSessionId'))
    },
    saveActiveUserSeatSelectionsToDatabase(state, targetIndex) {  // here, let the database be LocalStorage
      // map key: busTrip ID, value: selectionIndexes
      let finalData: { userId: number; selectionMap: string}
      let seatSelectionByBusIdMap: Map<number, Array<number>> = new Map([[state.activeBusTrip.id, state.activeBusTrip.selectedIndexes]])
      let selectionMapString: string

      // if found records in database (localStorage)
      if (state.seatSelectionByActiveUserId !== null) {
        const dbMap: Map<number, Array<number>> = new Map(JSON.parse(state.seatSelectionByActiveUserId.selectionMap))
        let selections: Array<number> | undefined = []
        selections = dbMap.get(state.activeBusTripId)
        // if found records by the busTrip id
        if (selections !== undefined) {
          if (!selections.includes(targetIndex)) selections.push(targetIndex) // new = add
          else selections.splice(selections.indexOf(targetIndex), 1)  // old = remove
          // marge active selection with database selection
          dbMap.set(state.activeBusTripId, selections)
          seatSelectionByBusIdMap = dbMap
        }
      }
      // initialize data if not already = no records found in database (localStorage)
      else state.seatSelectionByActiveUserId = {
        userId: -1,
        selectionMap: 'stringOfSeatSelectionByBusIdMap'
      }
      selectionMapString = JSON.stringify([...seatSelectionByBusIdMap])
      // user ID: activeUser ID, selectionMap: string of <above map>
      finalData = {userId: state.activeUserId, selectionMap: selectionMapString}

      // store
      localStorage.setItem('seatSelectionByActiveUserId', JSON.stringify(finalData))
      state.seatSelectionByActiveUserId = JSON.parse(<string>localStorage.getItem('seatSelectionByActiveUserId'))
    },
    saveAllUsersSeatSelectionsToDatabase(state) {  // here, let the database be LocalStorage
      // initialize data if not already
      if (state.seatSelectionOfAllUserList === null)
        state.seatSelectionOfAllUserList = new Array<{ userId: number; selectionMap: string }>()
      const indexOfActiveUser = state.seatSelectionOfAllUserList.findIndex(i => i.userId === state.seatSelectionByActiveUserId.userId)
      // if found no entry by active session ID, add selections
      if (indexOfActiveUser === -1) state.seatSelectionOfAllUserList.push(state.seatSelectionByActiveUserId)
      // else update selections
      else state.seatSelectionOfAllUserList[indexOfActiveUser] = state.seatSelectionByActiveUserId

      // store
      localStorage.setItem('seatSelectionOfAllUserList', JSON.stringify(state.seatSelectionOfAllUserList))
      state.seatSelectionOfAllUserList = JSON.parse(<string>localStorage.getItem('seatSelectionOfAllUserList'))
    }
  },
  actions: {
    updateFilterAction (ctx, value) {
      ctx.commit('setActiveFilter', value)
    },
    updateResultAction (ctx, value) {
      ctx.commit('setActiveSessionId')
      ctx.commit('setResult', value)
    },
    updateActiveBusTripAction (ctx, value) {
      ctx.commit('setActiveBusTrip', value)
    },
    updateSeatSelectionAction (ctx, value: { index: number; mode: string }) {
      if (value.mode === 'add') ctx.commit('addSeatSelection', value.index)
      else if (value.mode === 'remove') ctx.commit('removeSeatSelection', value.index)
      //else if (value.mode === 'reset') ctx.commit('clearSeatSelection')
      else return
      ctx.commit('saveActiveUserSeatSelectionsToDatabase', value.index)
      ctx.commit('saveAllUsersSeatSelectionsToDatabase')
    },
  },
  getters: {
    getBusTrips (state) {
      return state.busTripsFiltered
    },
    /* Active Bus Trip; let it's ID be 0 */
    getActiveBusTripIndex (state) {
      return state.busTripsFiltered.findIndex(i => i === state.activeBusTrip)
    },
    getAvailableSeatIndexesByBusId (state) {
      return state.activeBusTrip.getAvailableIndexes()
    },
    getBookedMaleSeatIndexesByBusId (state, getter) {
      /* mark seats that are currently selected by other user at the moment */
      const x = getter.getAllSeatSelectionsIndexListFromOtherUsersByBusId
      if (x.length > 0) return getter.getAllSeatSelectionsIndexListFromOtherUsersByBusId
      return state.activeBusTrip.bookedMaleIndexes
    },
    getBookedFemaleSeatIndexesByBusId (state) {
      return state.activeBusTrip.bookedFemaleIndexes
    },
    getBlockedSeatIndexesByBusId (state) {
      return state.activeBusTrip.blockedIndexes
    },
    getSoldMaleIndexesByBusId (state) {
      return state.activeBusTrip.soldMaleIndexes
    },
    getSoldFemaleIndexesByBusId (state) {
      return state.activeBusTrip.soldFemaleIndexes
    },
    getAllMaleIndexesByBusId (state, getter) {
      return [...getter.getSoldMaleIndexesByBusId, ...getter.getBookedMaleSeatIndexesByBusId]
    },
    getAllFemaleIndexesByBusId (state, getter) {
      return [...getter.getSoldFemaleIndexesByBusId, ...getter.getBookedFemaleSeatIndexesByBusId]
    },
    /* Global - Database */
    getSelectedIndexesByBusId: (state, getters)/* => (busTripIndex: number)*/ => {
      const allSelections = getters.getSeatSelectionsByActiveUserMap
      const indexes = allSelections.get(busTripIndex)
      // if found no entry by the id
      if (indexes === undefined) return []
      return indexes
    },
    getSeatSelectionsByActiveUserMap (state, getters) {
      return getters.getSeatSelectionByUserIdMap(state.activeUserId)
    },
    getSeatSelectionListFromOtherUsers (state) { // all users except me/ active SessionId
      if (state.seatSelectionOfAllUserList === null) return []
      return state.seatSelectionOfAllUserList?.filter(user => user.userId !== state.activeUserId)
    },
    // seats that are currently in Selected status by Other users
    getAllSeatSelectionsIndexListFromOtherUsersByBusId (state, getter)/* => (busTripIndex: number)*/ {
      const indexes: Array<number> = []
      const list: Array<{ userId: number; selectionMap: string }> = getter.getSeatSelectionListFromOtherUsers
      list.forEach(obj => {
        const map: Map<number, Array<number>> = new Map(JSON.parse(obj.selectionMap))
        const indexesByBusTrip: Array<number> | undefined = map.get(busTripIndex)
        if (indexesByBusTrip !== undefined) {
          indexes.push(...indexesByBusTrip)
        }
      })
      return indexes
    },
    getSeatSelectionByUserIdMap: (state) => (id: number) => {
      // first time loaded
      if (state.seatSelectionOfAllUserList === null) return new Map()
      const mySelection = state.seatSelectionOfAllUserList!!.find(user => user.userId === id)
      // no selection records made by the active user yet
      if (mySelection === undefined) return new Map()
      return new Map(JSON.parse(<string>mySelection.selectionMap))
    },
  },
  modules: {
  }
})
