# SELISE Front End - Test

## Technologies Used
`Vue`, `TypeScript`, `Vuetify`, `Vuex`, `RxJS`

## Project Description

Descriptions

> #### *Note:*
>**Active User** : **Browser Tab**/ `SessionStorage`
>
>**Database** : `LocalStorage`

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```